 CREATE TABLE client_address
 (
    id int(11) NOT NULL AUTO_INCREMENT,
    street varchar(100) NULL,
    number int(6) NULL,
    zip_code varchar(8) NOT NULL,
    city varchar(100) NOT NULL,
    state varchar(2) NOT NULL,
    PRIMARY KEY (id)
 ); 

 CREATE TABLE client
 (
    id int(11) NOT NULL AUTO_INCREMENT,
    name varchar(100) NOT NULL,
    email varchar(100) NOT NULL,
    cpf varchar(11) NOT NULL,
    id_address int(11) NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (id_address) REFERENCES client_address(id)
 );
