insert into client_address (street,number,zip_code,city,state) values ('Rua das Almas Penadas',96,'88095680','Florianópolis','SC');
insert into client(name, email,cpf,id_address) values ('Alan','alan@xxxx.com.br','04127017902',1);

insert into client_address (street,number,zip_code,city,state) values ('Rua do Ceifador',1051,'88060450','Florianópolis','SC');
insert into client(name, email,cpf,id_address) values('Rogério','rogerio.torres@gmail.com','08551346580',2);

insert into client_address (street,number,zip_code,city,state) values ('Rua de Outro Mundo',666,'88066666','Florianópolis','SC');
insert into client(name, email,cpf,id_address) values('Joao','eryc@xxxx.com','51725261294',3);