package br.com.archetype.persistence.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import br.com.archetype.model.Address;
import br.com.archetype.model.Client;

public class ClientRowMapper implements RowMapper<Client> {

    @Override
    public Client mapRow(ResultSet resultSet, int rowNum) throws SQLException {

        Client client = new Client();
        client.setId( resultSet.getLong("client_id") );
        client.setName( resultSet.getString("client_name") );
        client.setEmail( resultSet.getString("client_email") );
        client.setCpf( resultSet.getString("client_cpf") );

        Address address = new Address();
        address.setId( resultSet.getLong("address_id") );
        address.setStreet( resultSet.getString("address_street") );
        address.setNumber( resultSet.getInt("address_number") );
        address.setZipCode( resultSet.getString("address_zip_code") );
        address.setCity( resultSet.getString("address_city") );
        address.setState( resultSet.getString("address_state") );

        client.setAddress( address );

        return client;
    }
}
