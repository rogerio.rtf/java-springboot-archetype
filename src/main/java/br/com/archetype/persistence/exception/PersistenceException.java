package br.com.archetype.persistence.exception;


public class PersistenceException extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PersistenceException() {
        super();
    }

    public PersistenceException(String s) {
        super(s);
    }

    public PersistenceException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public PersistenceException(Throwable throwable) {
        super(throwable);
    }

    protected PersistenceException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }
}
