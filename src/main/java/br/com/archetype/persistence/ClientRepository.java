package br.com.archetype.persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.archetype.model.Address;
import br.com.archetype.model.Client;
import br.com.archetype.persistence.exception.PersistenceException;
import br.com.archetype.persistence.mapper.ClientRowMapper;

@Repository
public class ClientRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Transactional(rollbackFor = PersistenceException.class)
    public Client save(Client client) throws PersistenceException {

        try {
            client.getAddress().setId(  save(client.getAddress()).getId() );

            KeyHolder holder = new GeneratedKeyHolder();
            jdbcTemplate.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps = connection.prepareStatement("INSERT INTO client (name,email,cpf,id_address) VALUES (?,?,?,?)",
                            Statement.RETURN_GENERATED_KEYS);
                    ps.setString(1, client.getName());
                    ps.setString(2, client.getEmail());
                    ps.setString(3, client.getCpf());
                    ps.setLong(4, client.getAddress().getId());
                    return ps;
                }
            }, holder);
            if (holder != null && holder.getKey().longValue() > 0) {
                client.setId(holder.getKey().longValue());
            } else {
                throw new PersistenceException();
            }
            return client;
        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(),e);
        }
    }

    @Transactional(propagation = Propagation.MANDATORY,rollbackFor = PersistenceException.class)
    private Address save(Address address) throws PersistenceException {

        try{
            KeyHolder holder = new GeneratedKeyHolder();
            jdbcTemplate.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps = connection.prepareStatement("INSERT INTO client_address (street,number,zip_code,city,state) VALUES (?,?,?,?,?)",
                            Statement.RETURN_GENERATED_KEYS);
                    ps.setString(1, address.getStreet());
                    ps.setInt(2, address.getNumber());
                    ps.setString(3, address.getZipCode());
                    ps.setString(4, address.getCity());
                    ps.setString(5, address.getState());
                    return ps;
                }
            }, holder);
            if (holder != null && holder.getKey().longValue() > 0) {
                address.setId(holder.getKey().longValue());
            } else {
                throw new PersistenceException();
            }
            return address;
        } catch (Exception e){
            throw new PersistenceException(e.getMessage(),e);
        }
    }

    public List<Client> findAll() throws PersistenceException {
        try {
            return jdbcTemplate.query("SELECT " +
                            "c.id as client_id," +
                            "c.name as client_name," +
                            "c.email as client_email," +
                            "c.cpf as client_cpf," +
                            "ca.id as address_id," +
                            "ca.street as address_street," +
                            "ca.number as address_number," +
                            "ca.zip_code as address_zip_code," +
                            "ca.city as address_city," +
                            "ca.state as address_state" +
                            " FROM CLIENT c " +
                    "JOIN CLIENT_ADDRESS ca ON c.id_address = ca.id",
                    new ClientRowMapper());
        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(),e);
        }
    }

    public Client findById(Long id) throws PersistenceException {
        try {
            return jdbcTemplate.queryForObject("SELECT " +
                    "c.id as client_id," +
                    "c.name as client_name," +
                    "c.email as client_email," +
                    "c.cpf as client_cpf," +
                    "ca.id as address_id," +
                    "ca.street as address_street," +
                    "ca.number as address_number," +
                    "ca.zip_code as address_zip_code," +
                    "ca.city as address_city," +
                    "ca.state as address_state" +
                    " FROM CLIENT c " +
                    "JOIN CLIENT_ADDRESS ca ON c.id_address = ca.id" +
                    " WHERE c.id = ?",new Object[]{ id },new ClientRowMapper());
        } catch (Exception e) {
            throw new PersistenceException(e.getMessage(),e);
        }
    }
}
