package br.com.archetype.persistence;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.archetype.model.Customer;

public interface CustomerRepository extends MongoRepository<Customer, String> {

    public Customer findByFirstName(String firstName);
    public List<Customer> findByLastName(String lastName);
}