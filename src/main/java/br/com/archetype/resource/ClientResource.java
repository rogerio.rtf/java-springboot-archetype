package br.com.archetype.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.archetype.model.Client;
import br.com.archetype.service.ClientService;
import br.com.archetype.service.exception.ServiceException;

@RestController
public class ClientResource {

    @Autowired
    private ClientService clientService;

    @RequestMapping(value = "/v1/client", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findAll() throws ServiceException {

        List<Client> clients = clientService.findAll();
        return new ResponseEntity<>(clients, HttpStatus.OK);
    }

    @RequestMapping(value = "/v1/client/{id}", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findById(@PathVariable("id") Long id) throws ServiceException {

        return new ResponseEntity<>(clientService.findById(id), HttpStatus.OK);
    }
}
