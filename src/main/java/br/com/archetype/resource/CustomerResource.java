package br.com.archetype.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.archetype.model.Client;
import br.com.archetype.model.Customer;
import br.com.archetype.service.ClientService;
import br.com.archetype.service.CustomerService;
import br.com.archetype.service.exception.ServiceException;

@RestController
public class CustomerResource {

    @Autowired
    private CustomerService customerService;

    @RequestMapping(value = "/v1/customer", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> findAll() throws ServiceException {

        List<Customer> clients = customerService.findAll();
        return new ResponseEntity<>(clients, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/v1/customer", method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
    		consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> save(@RequestBody Customer customer) throws ServiceException {
        customerService.save(customer);
    	return new ResponseEntity<>(HttpStatus.OK);
    }
}
