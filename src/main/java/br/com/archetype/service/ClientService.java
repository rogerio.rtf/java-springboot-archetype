package br.com.archetype.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.archetype.model.Client;
import br.com.archetype.persistence.ClientRepository;
import br.com.archetype.persistence.exception.PersistenceException;
import br.com.archetype.service.exception.ServiceException;

@Service
public class ClientService {

    private static final Logger logger = LoggerFactory.getLogger(ClientService.class);

    @Autowired
    private ClientRepository clientRepository;

    public List<Client> findAll() throws ServiceException {

        try {
            return clientRepository.findAll();
        } catch (PersistenceException e) {
            throw new ServiceException(e.getMessage(),e);
        }
    }
    

    public Client findById(Long id) throws ServiceException {
        try {
            return clientRepository.findById(id);
        } catch (PersistenceException e) {
            throw new ServiceException(e.getMessage(),e);
        }
    }
}
