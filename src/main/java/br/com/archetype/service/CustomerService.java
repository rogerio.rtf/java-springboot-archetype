package br.com.archetype.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.archetype.model.Customer;
import br.com.archetype.persistence.CustomerRepository;
import br.com.archetype.service.exception.ServiceException;

@Service
public class CustomerService {

    private static final Logger logger = LoggerFactory.getLogger(CustomerService.class);

    @Autowired
    private CustomerRepository customerRepository;

    public List<Customer> findAll() throws ServiceException {
        return customerRepository.findAll();
    }

	public void save(Customer customer) {
		customerRepository.save(customer);
	}
}
